<?php

namespace App\UI\Desafio\Controller;

use App\Application\Desafio\FizzBuzzDTO;
use App\Application\Desafio\FizzBuzzUseCase;
use App\Domain\Desafio\Exception\InvalidDataFizzBuzzException;
use App\Infrastructure\Desafio\Form\FizzBuzzType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/desafio')]
class FizzBuzzController extends AbstractController
{
    public function __construct(readonly FizzBuzzUseCase $fizzBuzzUseCase)
    {
    }

    #[Route('/fizz/buzz', name: 'fizzbuzz', methods: ['POST'])]
    public function __invoke(Request $request): JsonResponse
    {
        try {
            $form = $this->createForm(FizzBuzzType::class, new FizzBuzzDTO());
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                /** @var FizzBuzzDTO $fizzBuzzDto */
                $fizzBuzzDto = $form->getData();
                $fizzBuzz = $this->fizzBuzzUseCase->__invoke($fizzBuzzDto);

                return new JsonResponse($fizzBuzz->getFizzbuzz(), Response::HTTP_OK);
            }

            return new JsonResponse(['errors' => $this->getErrorsFromForm($form)], Response::HTTP_BAD_REQUEST);
        } catch (InvalidDataFizzBuzzException $exception) {
            return new JsonResponse('Internal server error', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    private function getErrorsFromForm(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            /* @phpstan-ignore-next-line */
            $errors[$error->getCause()->getPropertyPath()] = $error->getMessage();
        }

        return $errors;
    }
}