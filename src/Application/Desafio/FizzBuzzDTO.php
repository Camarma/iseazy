<?php

declare(strict_types=1);

namespace App\Application\Desafio;

use Symfony\Component\Validator\Constraints as Assert;

class FizzBuzzDTO
{
    #[Assert\Positive(message: 'positive')]
    private int $startNumber;
    #[Assert\Expression(
        'this.getStartNumber() < this.getEndNumber()',
        message: 'endNumber must be greater than startNumber'
    )]
    private int $endNumber;

    public function __construct(int $startNumber = 0, int $endNumber = 0)
    {
        $this->startNumber = $startNumber;
        $this->endNumber = $endNumber;
    }

    public function getStartNumber(): int
    {
        return $this->startNumber;
    }

    public function setStartNumber(int $startNumber): void
    {
        $this->startNumber = $startNumber;
    }

    public function getEndNumber(): int
    {
        return $this->endNumber;
    }

    public function setEndNumber(int $endNumber): void
    {
        $this->endNumber = $endNumber;
    }


}