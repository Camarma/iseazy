<?php

declare(strict_types=1);

namespace App\Application\Desafio;

class FizzBuzzStringFactory
{
    const FIZZ_NUMBER = 3;
    const BUZZ_NUMBER = 5;
    const FIZZ = 'Fizz';
    const BUZZ = 'Buzz';

    public function __invoke(int $startNumber, int $endNumber): string
    {
        $fizzBuzz = [];

        for($i = $startNumber; $i <= $endNumber; $i++) {
            $fizzBuzz[] = $this->calculate($i);
        }

        return implode(', ', $fizzBuzz);
    }

    private function calculate(int $number): string 
    {
        if ($this->isFizzBuzzNumber($number)) {
            return self::FIZZ . self::BUZZ;
        }

        if ($this->isFizzNumber($number)) {
            return self::FIZZ;
        }

        if ($this->isBuzzNumber($number)) {
            return self::BUZZ;
        }   

        return (string) $number;
    }

    private function isFizzBuzzNumber(int $number): bool
    {
        return $number % (self::FIZZ_NUMBER * self::BUZZ_NUMBER) === 0;
    }

    private function isFizzNumber(int $number): bool
    {
        return $number % self::FIZZ_NUMBER === 0;
    }

    private function isBuzzNumber(int $number): bool
    {
        return $number % self::BUZZ_NUMBER === 0;
    }
}