<?php

declare(strict_types=1);

namespace App\Application\Desafio;

use App\Application\Shared\Clock;
use App\Domain\Desafio\Entity\FizzBuzz;
use App\Domain\Desafio\Exception\InvalidDataFizzBuzzException;
use App\Domain\Desafio\Repository\FizzBuzzRepositoryInterface;

class FizzBuzzUseCase
{
    public function __construct(
        readonly FizzBuzzStringFactory $fizzBuzzStringFactory,
        readonly FizzBuzzRepositoryInterface $fizzBuzzRepository,
        readonly Clock $clock)
    {

    }

    /**
     * @throws InvalidDataFizzBuzzException If FizzBuzz could not be created due to validation
     */
    public function __invoke(FizzBuzzDTO $fizzBuzzDTO): FizzBuzz
    {
        $fizzBuzzString = $this->fizzBuzzStringFactory->__invoke($fizzBuzzDTO->getStartNumber(), $fizzBuzzDTO->getEndNumber());

        $fizzBuzz = new FizzBuzz(
            $fizzBuzzDTO->getStartNumber(),
            $fizzBuzzDTO->getEndNumber(),
            $fizzBuzzString,
            $this->clock->nowImmutable()
        );

        $this->fizzBuzzRepository->save($fizzBuzz);

        return $fizzBuzz;
    }
}