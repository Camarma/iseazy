<?php

namespace App\Application\Shared;

interface Clock
{
    public function nowImmutable(): \DateTimeImmutable;
}