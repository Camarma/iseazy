<?php

namespace App\Domain\Desafio\Entity;

use App\Domain\Desafio\Exception\InvalidDataFizzBuzzException;
use App\Domain\Shared\Entity\Validatable;
use App\Infrastructure\Desafio\DoctrineFizzBuzzRepository;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: DoctrineFizzBuzzRepository::class)]
class FizzBuzz implements Validatable
{
    const MAX_LENGTH_FIZZ_BUZZ = 255;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int $id;

    #[ORM\Column]
    private int $startNumber;

    #[ORM\Column]
    private int $endNumber;

    #[ORM\Column]
    private DateTimeImmutable $createdAt;

    #[ORM\Column(length: self::MAX_LENGTH_FIZZ_BUZZ)]
    private string $fizzbuzz;

    public function __construct(int $startNumber, int $endNumber, string $fizzbuzz, DateTimeImmutable $createdAt)
    {
        $this->startNumber = $startNumber;
        $this->endNumber = $endNumber;
        $this->fizzbuzz = $fizzbuzz;
        $this->createdAt = $createdAt;

        $errors = $this->validate();

        if (count($errors) > 0) {
            throw new InvalidDataFizzBuzzException();
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartNumber(): int
    {
        return $this->startNumber;
    }

    public function setStartNumber(int $startNumber): static
    {
        $this->startNumber = $startNumber;

        return $this;
    }

    public function getEndNumber(): int
    {
        return $this->endNumber;
    }

    public function setEndNumber(int $endNumber): static
    {
        $this->endNumber = $endNumber;

        return $this;
    }

    public function getCreatedAt(): DateTimeImmutable
    {
        return $this->createdAt;
    }

    public function setCreatedAt(DateTimeImmutable $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getFizzbuzz(): string
    {
        return $this->fizzbuzz;
    }

    public function setFizzbuzz(string $fizzbuzz): static
    {
        $this->fizzbuzz = $fizzbuzz;

        return $this;
    }

    public function validate(): array
    {
        $errors = [];

        if(strlen($this->getFizzbuzz()) > self::MAX_LENGTH_FIZZ_BUZZ) {
            $errors['fizzbuzz'] = sprintf(
                'Length of fizzbuzz field is %d. Must not be greater than %s',
                $this->getFizzbuzz(),
                self::MAX_LENGTH_FIZZ_BUZZ
            );
        }

        return $errors;
    }
}
