<?php

namespace App\Domain\Desafio\Exception;

use Exception;
use Throwable;

class InvalidDataFizzBuzzException extends Exception
{
    public function __construct()
    {
        parent::__construct('Invalid format data');
    }
}