<?php

declare(strict_types=1);

namespace App\Domain\Desafio\Repository;

use App\Domain\Desafio\Entity\FizzBuzz;

interface FizzBuzzRepositoryInterface
{
    public function save(FizzBuzz $fizzBuzz): void;
}