<?php

namespace App\Domain\Shared\Entity;

interface Validatable
{
    public function validate(): array;
}