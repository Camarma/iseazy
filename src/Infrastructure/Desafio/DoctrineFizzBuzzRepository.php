<?php

declare(strict_types=1);

namespace App\Infrastructure\Desafio;

use App\Domain\Desafio\Entity\FizzBuzz;
use App\Domain\Desafio\Repository\FizzBuzzRepositoryInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<FizzBuzz>
 *
 * @method FizzBuzz|null find($id, $lockMode = null, $lockVersion = null)
 * @method FizzBuzz|null findOneBy(array $criteria, array $orderBy = null)
 * @method FizzBuzz[]    findAll()
 * @method FizzBuzz[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DoctrineFizzBuzzRepository extends ServiceEntityRepository implements FizzBuzzRepositoryInterface
{
    private EntityManagerInterface $em;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FizzBuzz::class);
        $this->em = $this->getEntityManager();
    }

    public function save(FizzBuzz $fizzBuzz): void
    {
        $this->em->persist($fizzBuzz);
        $this->em->flush();
    }

    public function delete(FizzBuzz $fizzBuzz): void
    {
        $this->em->remove($fizzBuzz);
        $this->em->flush();
    }
}
