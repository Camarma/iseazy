<?php

declare(strict_types=1);

namespace App\Infrastructure\Desafio\Form;

use App\Application\Desafio\FizzBuzzDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FizzBuzzType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('startNumber', NumberType::class)
            ->add('endNumber', NumberType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => FizzBuzzDTO::class,
        ]);
    }

    public function getName(): string {
        return "";
    }

    public function getBlockPrefix(): string {
        return "";
    }
}