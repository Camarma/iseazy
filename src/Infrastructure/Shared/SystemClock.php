<?php declare(strict_types=1);

namespace App\Infrastructure\Shared;

use App\Application\Shared\Clock;
use DateTimeImmutable;

class SystemClock implements Clock
{

    public function nowImmutable(): DateTimeImmutable
    {
        return new DateTimeImmutable();
    }
}