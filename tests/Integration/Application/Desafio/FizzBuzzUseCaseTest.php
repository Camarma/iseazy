<?php

namespace App\Tests\Integration\Application\Desafio;

use App\Application\Desafio\FizzBuzzDTO;
use App\Application\Desafio\FizzBuzzStringFactory;
use App\Application\Desafio\FizzBuzzUseCase;
use App\Application\Shared\Clock;
use App\Domain\Desafio\Exception\InvalidDataFizzBuzzException;
use App\Infrastructure\Desafio\DoctrineFizzBuzzRepository;
use App\Infrastructure\Shared\SystemClock;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class FizzBuzzUseCaseTest extends KernelTestCase
{
    private FizzBuzzStringFactory $fizzBuzzStringFactory;
    private DoctrineFizzBuzzRepository $fizzBuzzRepository;
    private SystemClock $clock;

    protected function setUp(): void
    {
        $this->fizzBuzzStringFactory = self::getContainer()->get(FizzBuzzStringFactory::class);
        $this->fizzBuzzRepository = self::getContainer()->get(DoctrineFizzBuzzRepository::class);
        $this->clock = self::getContainer()->get(Clock::class);
    }

    protected function tearDown(): void
    {
        unset(
            $this->fizzBuzzStringFactory,
            $this->fizzBuzzRepository,
        );

        parent::tearDown();
    }

    public function testShouldBeCreateFizzBuzz(): void
    {
        $sut = new FizzBuzzUseCase($this->fizzBuzzStringFactory, $this->fizzBuzzRepository, $this->clock);

        $fizzBuzz = $sut->__invoke(new FizzBuzzDTO(1, 10));

        $fizzBuzzStorage = $this->fizzBuzzRepository->find($fizzBuzz->getId());

        $this->assertEquals($fizzBuzz, $fizzBuzzStorage);

        $this->fizzBuzzRepository->delete($fizzBuzz);
    }
}