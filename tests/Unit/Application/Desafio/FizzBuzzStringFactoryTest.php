<?php

namespace App\Tests\Unit\Application\Desafio;

use App\Application\Desafio\FizzBuzzStringFactory;
use PHPUnit\Framework\TestCase;

class FizzBuzzStringFactoryTest extends TestCase
{
    protected function setUp(): void
    {
        parent::__construct();
    }

    public function testShouldReturnFizzBuzzString(): void
    {
        $sut = new FizzBuzzStringFactory();
        $expected = 'FizzBuzz, 31, 32, Fizz, 34, Buzz, Fizz, 37, 38, Fizz, Buzz, 41, Fizz, 43, 44, FizzBuzz, 46, 47, Fizz, 49, Buzz, Fizz, 52, 53, Fizz, Buzz, 56, Fizz, 58, 59, FizzBuzz, 61, 62, Fizz, 64, Buzz, Fizz, 67';

        $this->assertEquals($expected, $sut->__invoke(30, 67));
    }
}