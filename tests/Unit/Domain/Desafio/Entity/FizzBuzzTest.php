<?php

namespace App\Tests\Unit\Domain\Desafio\Entity;

use App\Domain\Desafio\Entity\FizzBuzz;
use App\Domain\Desafio\Exception\InvalidDataFizzBuzzException;
use DateTimeImmutable;
use PHPUnit\Framework\TestCase;

class FizzBuzzTest extends TestCase
{
    public function testValidEntity(): void
    {
        $this->assertInstanceOf(
            FizzBuzz::class,
            new FizzBuzz(
                1,
                10,
                "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz",
                new DateTimeImmutable()
            )
        );
    }

    /**
     * @dataProvider invalidEntities
     */
    public function testInvalidEntity(int $startNumber, int $endNumber, string $fizzbuzz, DateTimeImmutable $createdAt):void
    {
        $this->expectException(InvalidDataFizzBuzzException::class);

        new FizzBuzz(
            $startNumber,
            $endNumber,
            $fizzbuzz,
            $createdAt
        );
    }

    public function invalidEntities(): array
    {
        return [
            [1, 80, "FizzBuzz, 31, 32, Fizz, 34, Buzz, Fizz, 37, 38, Fizz, Buzz, 41, Fizz, 43, 44, FizzBuzz, 46, 47, Fizz, 49, Buzz, Fizz, 52, 53, Fizz, Buzz, 56, Fizz, 58, 59, FizzBuzz, 61, 62, Fizz, 64, Buzz, Fizz, 67, 68, FizzBuzz, 31, 32, Fizz, 34, Buzz, Fizz, 37, 38, Fizz, Buzz, 41, Fizz, 43, 44, FizzBuzz, 46, 47, Fizz, 49, Buzz, Fizz, 52, 53, Fizz, Buzz, 56, Fizz, 58, 59, FizzBuzz, 61, 62, Fizz, 64, Buzz, Fizz, 67, 68", new DateTimeImmutable()]
        ];
    }
}