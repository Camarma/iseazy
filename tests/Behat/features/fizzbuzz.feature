Feature:
    In order to prove that the FizzBuzz endpoint is correctly worked
    As a user
    I want to generate a fizzbuzz string

    Scenario: Create FizzBuzz
        When I send a POST request to "/desafio/fizz/buzz" with this body:
            | startNumber   | 1     |
            | endNumber     | 10    |
        Then the response status code should be 200
        And the response should be "1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz"

    Scenario: Do not create FizzBuzz when endNumber is greater than startNumber
        When I send a POST request to "/desafio/fizz/buzz" with this body:
            | startNumber   | 10 |
            | endNumber     | 1  |
        Then the response status code should be 400
