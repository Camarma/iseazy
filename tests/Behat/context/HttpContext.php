<?php

declare(strict_types=1);

namespace App\Tests\Behat\context;

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\TableNode;
use PHPUnit\Framework\Assert;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * This context class contains the definitions of the steps used by the demo
 * feature file. Learn how to get started with Behat and BDD on Behat's website.
 *
 * @see http://behat.org/en/latest/quick_start.html
 */
final class HttpContext implements Context
{
    /** @var KernelInterface */
    private $kernel;

    /** @var Response|null */
    private $response;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * @When I send a POST request to :path with this body:
     */
    public function sendRequestWhitBody(string $path, TableNode $body): void
    {
        $this->response = $this->kernel->handle(Request::create($path, 'POST', $body->getRowsHash()));
    }

    /**
     * @Then the response status code should be :responseCode
     */
    public function theResponseStatusCodeShouldBe($responseCode)
    {
        Assert::assertEquals($responseCode, $this->response->getStatusCode());
    }

    /**
     * @Then the response should be :value
     */
    public function theResponseEntryShouldBe($value)
    {
        Assert::assertEquals($value, json_decode($this->response->getContent(),  true));
    }
}
