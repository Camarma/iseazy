<p align="center"><a href="https://www.iseazy.com/es/" target="_blank"><img src="https://www.softwaredoit.es/logotipos/iseazy.jpg?t=2023-01-27_14_54_28" width="200" alt="Iseazy"></a></p>


## Requirements

- Docker
- Copy `.env.local.dist` file as `.env.local` in the root path of the project

## Instalation

Steps to build docker containers and generate iseazy database.

```bash
docker compose up --build
docker exec -it iseazy-php composer install
docker exec -it iseazy-php bin/console doctrine:schema:update
```

## Api endpoints

- Generate FizzBuzz.

  <span style="color: orange">POST</span> http://localhost/desafio/fizz/buzz

  Call example:
    ```bash
    curl --location --request POST 'http://localhost/desafio/fizz/buzz' \
    --form 'startNumber=8' \
    --form 'endNumber=12'
    ```

## Testing

```bash
docker exec -it iseazy-php composer test
```
